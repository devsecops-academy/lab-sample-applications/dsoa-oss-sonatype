# building stage
FROM maven:3-jdk-8 AS build

WORKDIR /app
COPY app .
RUN [ "mvn", "-Drat.skip=true", "package" ]

# running stage
FROM tomcat:9-jdk8

COPY --from=build /app/target/struts2-rest-showcase.war /usr/local/tomcat/webapps/
